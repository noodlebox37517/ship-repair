﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Base_Object : NetworkBehaviour
{

    private Level_Controller _levelObj;
    public Level_Controller levelObj
    {
        get
        {
            return _levelObj;
        }
        set
        {
            _levelObj = value;
            grid = levelObj.ClosestGrid(this.transform.position);
        }
    }

    protected Grid.GridHeight DefaultGridHeight = Grid.GridHeight.On;
    public Grid grid;

    // Use this for initialization
    protected virtual void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual void PlaceGrid()
    {
        Grid targrid = levelObj.ClosestGrid(this.transform.position);
        if (targrid.AcceptObject)
        {
            if (targrid.PlaceObject(this.gameObject, DefaultGridHeight))
            {
                this.transform.position = new Vector3(targrid.PosGrid.x, targrid.PosGrid.y, targrid.PosGrid.z - 1f);
                Debug.Log(targrid.NumGrid);
                grid = targrid;
            }
            else
            {
                Debug.Log("occupied, place next to not implemented!" + targrid.NumGrid);
                this.name = " Stacked";
                grid = targrid;
            }
        }
    }

    public virtual void CmdUpdateGrid()
    {

    }
}

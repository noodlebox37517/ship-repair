﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cable : MonoBehaviour {
    public GameObject aboveObject;
    public List<GameObject> GivesPowerto = new List<GameObject>();
    public bool haspower = false;
    public bool powerchange = false;
    public Sprite[] sprites;
    private bool _connected = true;
    public bool connected
    {
        get
        {
            return _connected;
        }
        set
        {
            _connected = value;
            if (value)
            {
                this.GetComponent<SpriteRenderer>().sprite = sprites[1];
            }
            else
            {
                this.GetComponent<SpriteRenderer>().sprite = sprites[0];
            }
        }
    }
    public Grid grid;
    public Level_Controller levelObj;
    // Use this for initialization

    void Start() {
        grid = levelObj.ClosestGrid(this.transform.position);
        connected = connected;
    }

    // Update is called once per frame
    void Update() {
        if (powerchange)
        {
            powerchange = false;
            if (haspower) {
                GivePower();
            }
            else
            {
                TakePower();
            }
        }
    }
    void FixedUpdate()
    {

    }
    public void SetPower(bool power)
    {
        haspower = power;
        if (power)
        {
            GivePower();
        }
        else
        {
            TakePower();
        }
    }
    public void GivePower()
    {
        Grid[] grids = levelObj.GetNieghbourGrids(grid);
        foreach (Grid i in grids)
        {
            if (i.objectUnderGrid != null && i.objectUnderGrid != this.gameObject)
            {
                if (i.objectUnderGrid.GetComponent<Power>() != null)
                {
                    if (i.objectUnderGrid.GetComponent<Power>().gameObject.GetComponent<Power>().powerOn==false) {
                        if (i.objectUnderGrid.GetComponent<Cable>()== null || i.objectUnderGrid.GetComponent<Cable>().connected) {
                            GivesPowerto.Add(i.objectUnderGrid);
                            i.objectUnderGrid.GetComponent<Power>().powerProvider = this.gameObject;
                            i.objectUnderGrid.GetComponent<Power>().gameObject.GetComponent<Power>().setpower(true);
                        }
                    }
                }
            }
        }
        if (grid.objectInGrid != null) {
            if (grid.objectInGrid.GetComponent<Door>() != null)
            {
                GivesPowerto.Add(grid.objectInGrid);
                grid.objectInGrid.gameObject.GetComponent<Door>().powerOn = true;
                //Debug.Log("object given power above " + grid.objectInGrid.gameObject.name);
            }
        }
        powerchange = false;

    }
    public void TakePower()
    {
        foreach (GameObject i in GivesPowerto)
        {
            i.GetComponent<Power>().powerOn = false;
            GivesPowerto.Remove(i);

        }
    }

        public void TakePower(GameObject i)
    {
            i.GetComponent<Power>().powerOn = false;
            GivesPowerto.Remove(i);
    }
    public void ChangeConnect()
    {
        //change state of cable to connected or unconnected
        connected = !connected;
        if (connected)
        {
            //check for nieghbours with power and ask for it if true
            // once given normal pwoer process commences nno further action neeeded    
                }
        else
        {
            
            TakePower();
            //tell power giver to remove this from list of powered objects.
            if (this.GetComponent<Power>().powerProvider != null) 
            this.GetComponent<Power>().powerProvider.GetComponent<Cable>().TakePower(this.gameObject);
        }
    }
}

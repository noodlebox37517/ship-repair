﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour {
    public bool mainMenu = false;
    public bool gameMenu = false;
    public GameObject MiddleMenu = null;
    public List<GameObject> menuObj = new List<GameObject>();
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        if (gameMenu == true)
        {
            //check for input
            if (Input.GetKeyDown(KeyCode.Escape)){
                foreach (GameObject g in menuObj)
                {
                    if (g.GetComponent<CanvasRenderer>().GetAlpha() > 0)
                    {
                        g.GetComponent<CanvasRenderer>().SetAlpha(0f);
                        g.GetComponent<Image>().raycastTarget = false;
                    }
                    else {
                        g.GetComponent<CanvasRenderer>().SetAlpha(1f);
                        g.GetComponent<Image>().raycastTarget = true;
                    }
                }
               
            }
        }
    }
    //main menu buttons
    public void StoryModeButton()
    {
        SceneManager.LoadScene("Lobby", LoadSceneMode.Additive);
        Debug.Log("button pushed");
    }
    public void ContinousModeButton()
    {
        SceneManager.LoadScene("Lobby", LoadSceneMode.Additive);
        Debug.Log("button pushed");
    }
    public void ScoresButton()
    {
        Debug.Log("button pushed");
    }
    public void OptionsButton()
    {
        Debug.Log("button pushed");
    }
    public void ExitButton()
    {
        Application.Quit();
        Debug.Log("button pushed");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_Controller : NetworkBehaviour
{
    public Level_Controller levelObj;
    public GameObject ItemLocation;
    Random rnd = new Random();
    [SyncVar]
    public GameObject Item;

    public delegate void ChangeItemDelegate(GameObject changeItem);
    [SyncEvent]
    public event ChangeItemDelegate EventChangeItem;
    
    public float playerSpeed = 1.5f;
    public float rotation = 1;
    private float internalCD;
    //public float useCD = 0.5f;

    // Use this for initialization
    void Start () {
        if (NetworkClient.active)
        {
            EventChangeItem += Player_Controller_EventChangeItem;
        }
        if (isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
        }
    }

    private void Player_Controller_EventChangeItem(GameObject changeItem)
    {
        Item = changeItem;
    }
    public int startingPlayerHeath = 100;
    public int currentPlayerHealth;
    bool dead = false;
    private NetworkStartPosition[] spawnPoints;
    
    public float useCD = 0.5f;

    // Use this for initialization
    // Update is called once per frame
    public override void OnStartLocalPlayer()
    {
        // load correct sprite
        CmdFindGame_Controller();
        ItemLocation = transform.FindChild("Item").gameObject;
        currentPlayerHealth = startingPlayerHeath;
    }
    private void Reset()
    {
    }

    [Command]
    void CmdFindGame_Controller()
    {
        GameObject levelObject = GameObject.FindGameObjectWithTag("GameController");
        levelObj = levelObject.GetComponent<Level_Controller>();
    }

    [Command]
    void CmdDrop()
    {
        // This [Command] code is run on the server!
        //try infront then try Under
        //check if the user is ontop of a item to pick it up
        Grid underGrid = levelObj.ClosestGrid(this.transform.position);
        Grid targrid;
        if (GridInFront(underGrid, out targrid) && targrid.AcceptObject == true && targrid.objectOnGrid == null) //if statment is largely not needed, but in as safety atm
        {
            placeOnTile(targrid);
        }
        else if (underGrid.AcceptObject == true && underGrid.objectOnGrid == null)
        {
            placeOnTile(underGrid);
        }
        else
        {
            Debug.Log("cant Dropped Item");
        }
    }

    void placeOnTile(Grid location)
    {

        location.GiveObject(Item);
        EventChangeItem(null);
        //Item = null;
        if (Item.GetComponent<Item_Object>() != null)
        {
            Item.GetComponent<Item_Object>().Placed(this.gameObject);
        }
        Item = null;
    }

    [Command]
    void CmdPickup()
    {
        // This [Command] code is run on the server!
        //try under then try infront
        //check if the user is ontop of a item to pick it up
        Grid underGrid = levelObj.ClosestGrid(this.transform.position);
        Grid targrid;
        if (GridInFront(underGrid, out targrid)){
            //targrid.NumGrid 
            if (targrid.objectOnGrid != null && (targrid.objectOnGrid.GetComponent<Item_Object>()) != null) //if statment is largely not needed, but in as safety atm
            {
                GameObject newItem;
                targrid.TakeObject(out newItem);
                EventChangeItem(newItem);
                newItem.transform.parent = ItemLocation.transform;
                newItem.transform.position = new Vector3(ItemLocation.transform.position.x, ItemLocation.transform.position.y, -1);
                newItem.GetComponent<SpriteRenderer>().enabled = true;
                Debug.Log("Player Picked up Item");
                return;

            } else if (targrid.objectInGrid != null && targrid.objectInGrid.GetComponent<SpawnPoint>() != null)
            {
                GameObject newItem = null;
                targrid.objectInGrid.GetComponent<SpawnPoint>().interact(ref newItem);
                EventChangeItem(newItem);
                newItem.transform.parent = ItemLocation.transform;
                newItem.transform.position = new Vector3(ItemLocation.transform.position.x, ItemLocation.transform.position.y, -1);
                Debug.Log("Player got spawned Item");
                return;
            }
            else if (targrid.objectInGrid != null && targrid.objectInGrid.GetComponent<Door>() != null) //added powered check
            {
                //door force open operation
                return;
            }
            else if (targrid.objectInGrid != null && targrid.objectInGrid.GetComponent<Power_Generator>() != null) // added slave check
            {
                //power gen operation
                return;
            }
        }
         if(underGrid.objectOnGrid != null && (underGrid.objectOnGrid.GetComponent<Item_Object>()) != null) //if statment is largely not needed, but in as safety atm
        {
            GameObject newItem;
            underGrid.TakeObject(out newItem);
            EventChangeItem(newItem);
            newItem.transform.parent = ItemLocation.transform;
            newItem.transform.position = new Vector3(ItemLocation.transform.position.x, ItemLocation.transform.position.y, -1);
            newItem.GetComponent<SpriteRenderer>().enabled = true;
            Debug.Log("Player Picked up Item");
        }else 
        {
            Debug.Log("Player tried to pick somthing up, nothing here to pickup");
        }
    }

    [Command]
    void CmdUse()
    {
        Tool toolScript =  Item.GetComponent<Tool>();
        Part partScript = Item.GetComponent<Part>();
        if (toolScript != null)
        {
            Grid underGrid = levelObj.ClosestGrid(this.transform.position);
            Grid targrid;
            if (GridInFront(underGrid, out targrid))
            {
                toolScript.use(targrid);
            }
        }else if (partScript !=null)
        {
            Grid underGrid = levelObj.ClosestGrid(this.transform.position);
            Grid targrid;
                //if it is a part it needs to be placed / used up when activated
                if (GridInFront(underGrid, out targrid) && partScript.place(targrid))
                {
                    Item.GetComponent<SpriteRenderer>().enabled = false;
                    placeOnTile(targrid);
                }
            
            else if (partScript.place(underGrid))
            {
                Item.GetComponent<SpriteRenderer>().enabled = false;
                placeOnTile(underGrid);
            }
        }
    }



    private bool GridInFront (Grid gridUnder, out Grid gridInFront)
    {
        float x = gridUnder.NumGrid.x - Mathf.Round(transform.up.x);
        float y = gridUnder.NumGrid.y - Mathf.Round(transform.up.y);

        //if (levelObj.levelGrid.Length >= (int)x)
        //{
        //    x = levelObj.levelGrid.Length - 1;
        //}
        //if (levelObj.levelGrid.GetUpperBound(1) >= (int)y)
        //{
        //    y = levelObj.levelGrid.GetUpperBound(1);
        //}

        try
        {

            gridInFront = levelObj.levelGrid[(int)x, (int)y];
            return true;
        }
        catch
        {
            gridInFront = null;
            return false;
        }
    }

    void FixedUpdate () {
        {
            //check is localplayer
            if (!isLocalPlayer)
                return;
            //movement
            Vector2 movement = new Vector2(
                Input.GetAxis("Horizontal"),
                Input.GetAxis("Vertical")
                );
            
            this.GetComponent<Rigidbody2D>().velocity = new Vector2( movement.x * playerSpeed, movement.y * playerSpeed);

            //only rotate if moving
            if (movement.x != 0 || movement.y != 0) transform.up = Vector3.Normalize(-movement);
            if (Input.GetKeyDown(KeyCode.X) && Item != null)
            {
                Debug.Log("Drop");
                CmdDrop();
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                Debug.Log("tried to Drop nothing");
            }

            else if (Input.GetKeyDown(KeyCode.Space) && Item == null)
            {
                Debug.Log("Pickup");
                CmdPickup();
            }
            else if (Input.GetKey(KeyCode.Space) && Item != null)
            {
                Tool toolScript = Item.GetComponent<Tool>();
                
                if (Time.time > internalCD)
                {
                Debug.Log("Use");
                CmdUse();
                    if (toolScript != null) {
                        internalCD = Time.time + toolScript.coolDown;
                    }
                }

            }
            playerRespawn();
        }
    }

    // Player damage
    void playerRespawn()
    {
        if (dead == true)
        {

            dead = false;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length - 1)].gameObject.transform.position;
            this.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public void playerTakeDamage(int damageAmount)
    {
        currentPlayerHealth -= damageAmount;

        if (currentPlayerHealth <= 0)
        {
            dead = true;
        }
    }
}

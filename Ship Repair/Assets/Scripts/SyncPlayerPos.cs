﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SyncPlayerPos : NetworkBehaviour {
    [SyncVar]
    private Vector3 syncPos;

    [SerializeField]
    Transform myTransform;
    [SerializeField]
    float lerpRate = .5f;


    private Vector3 lastPos;
    public float threshold = 0.5f;

	void FixedUpdate () {

        transmitPosition();
        LerpPosition();
	}
    void LerpPosition() {
        if(!isLocalPlayer)
        {
            myTransform.position = Vector3.Lerp(myTransform.position,syncPos,lerpRate);
        }
    }

    [Command]
    void CmdProvidePos(Vector3 pos)
    {
        syncPos = pos;
    }

    [ClientCallback]
    void transmitPosition()
    {
        if(isLocalPlayer&& Vector3.Distance(myTransform.position,lastPos) > threshold)
        {
            CmdProvidePos(myTransform.position);
            lastPos = myTransform.position;
        }
    }
}

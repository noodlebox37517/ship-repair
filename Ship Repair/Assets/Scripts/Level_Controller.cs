﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Level_Controller : NetworkBehaviour {
    public bool testLevel = false;
    public bool usemapgen = false;
    [Tooltip("Int only, max x by max y of grid 0 indexed")]
    public Vector2 gridSize = new Vector2(28, 16);
    private float gridSpacing;
    private Vector2 gridOffset;
    private float adjustedscreenwidth;
    private int[,] levelUnderLocations;
    private int[,] levelObjectLocations;
    private int[,] levelItemLocations;
    private int[,] levelMasterLocations;
    [Tooltip("grid of the level,Does not need to be set")]
    public Grid[,] levelGrid;
    [Header("Prefabs")]
    public GameObject wallPrefab;
    public GameObject floorPrefab;
    public GameObject[] ItemPrefab;
    public GameObject[] underPrefabs;

    [Header ("Power Gen Shapes")]
    public List<Shape> shapes = new List<Shape>();
    public int genShapesMissing = 0;

    [Header("Cable Settings")]
    protected List<GameObject> cableList = new List<GameObject>();
    public int cablestobreak = 0;
    public float elapsedTime;
    public float startTime;
    public Text elapsedTimeDisplay;
    private float KEEP_ASPECT = 16 / 9f;
    // Use this for initialization

    public override void OnStartServer()
    {
        CmdGenerateLevel();
    }
	

    [Command]
    public void CmdGenerateLevel()
    {
        levelUnderLocations = new int[(int)(gridSize.x), (int)(gridSize.y)];
        levelObjectLocations = new int[(int)(gridSize.x), (int)(gridSize.y)];
        levelItemLocations = new int[(int)(gridSize.x), (int)(gridSize.y)];
        levelMasterLocations = new int[(int)(gridSize.x), (int)(gridSize.y)];

        levelGrid = new Grid[(int)(gridSize.x), (int)(gridSize.y)];
        gridSpacing = (Screen.height - (Screen.height % (gridSize.y))) / (gridSize.y);
        if (Screen.width / gridSpacing >= gridSize.x)
        {
            gridSpacing = (Screen.height - (Screen.height % (gridSize.y))) / (gridSize.y);
        }
        else
        {
            gridSpacing = (Screen.width - (Screen.width % (gridSize.x))) / (gridSize.x);
        }
        gridOffset = new Vector2(Screen.width % gridSpacing / 2, Screen.height % gridSpacing / 2);

        Debug.Log(gridSpacing + " " + Screen.height);
        //test
        //temp variables for initliziation
        // this is here so that the maingen can get its slave gens before runtime
        List<GameObject> SlaveGens = new List<GameObject>();
        GameObject MainGen = null;
        #region test setup

        // sets grid position
        for (int x = 0; x < levelGrid.GetLength(0); x++)
        {
            for (int y = 0; y < levelGrid.GetLength(1); y++)
            {
                levelGrid[x, y] = new Grid();
                levelGrid[x, y].SetValues(
                    new Vector2(x, y), Camera.main.ScreenToWorldPoint(new Vector3(
                        (x * gridSpacing) + gridSpacing / 2 + gridOffset.x,
                        (y * gridSpacing) + gridSpacing / 2 + gridOffset.y,
                        12
                        )), true);
            }
        }
        if (!usemapgen)
        {
            //sets objects in grid
            for (int x = 0; x < levelObjectLocations.GetLength(0); x++)
            {
                for (int y = 0; y < levelObjectLocations.GetLength(1); y++)
                {
                    if (x != 14 && (y == 2 || y == 13))
                    {
                        levelObjectLocations[x, y] = 1;

                    }
                    else if ((y > 2 && y < 13) || (y < 2 || y > 13) || (x == 14 && (y == 2 || y == 13)))
                    {
                        levelObjectLocations[x, y] = 2;
                    }

                }
            }
            for (int x = 0; x < levelItemLocations.GetLength(0); x++)
            {
                for (int y = 0; y < levelItemLocations.GetLength(1); y++)
                {
                    if (y == 0 && x == 0)
                        levelItemLocations[x, y] = 1;
                    else if (y == 6)
                        levelItemLocations[x, y] = 2;
                    else if ((y == 2 || y == 13) && x == 14 || y == 8 && x == 12)
                        levelItemLocations[x, y] = 3;
                    else if ((y == 9 || y == 10) && (x == 14 || x == 15) || (y == 8 && x == 15))
                        levelItemLocations[x, y] = 4;
                    else if (y == 8 && x == 14)
                        levelItemLocations[x, y] = 5;
                    else if ((y == 0 && x == 1))
                        levelItemLocations[x, y] = 6;
                    else if ((y == 0 && x == 2))
                        levelItemLocations[x, y] = 7;

                }
            }
            for (int x = 0; x < levelUnderLocations.GetLength(0); x++)
            {
                for (int y = 0; y < levelUnderLocations.GetLength(1); y++)
                {
                    if (y == 8 && (x == 13 || x == 12) || x == 14)
                        levelUnderLocations[x, y] = 1;

                }
            }
        }
        #endregion
        //inizializes objects in world and sets grid properties
        // 0 == nothing /default
        // 1 == wall
        // 2 == floor
        if (testLevel == true)
        {
            if (usemapgen)
                this.GetComponent<Map_Generator>().mapGeneration(ref levelMasterLocations);
            for (int x = 0; x < levelMasterLocations.GetLength(0); x++)
            {
                for (int y = 0; y < levelMasterLocations.GetLength(1); y++)
                {
                    switch (levelMasterLocations[x, y])
                    {
                        case 1:
                            levelObjectLocations[x, y] = 1;
                            break;
                        case 2:
                            levelObjectLocations[x, y] = 2;
                            break;
                        case 3:
                            levelObjectLocations[x, y] = 3;
                            levelUnderLocations[x, y] = 1;
                            break;
                        case 4:
                            levelObjectLocations[x, y] = 2;
                            levelUnderLocations[x, y] = 1;
                            break;
                        case 5:
                            levelObjectLocations[x, y] = 4;
                            break;
                        case 6:
                            levelObjectLocations[x, y] = 5;
                            break;
                        case 7:
                            levelItemLocations[x, y] = 2;
                            levelObjectLocations[x, y] = 2;
                            break;
                        case 8:
                            levelItemLocations[x, y] = 2;
                            levelObjectLocations[x, y] = 2;
                            levelUnderLocations[x, y] = 1;
                            break;
                        case 9:
                            levelItemLocations[x, y] = 3;
                            levelObjectLocations[x, y] = 2;
                            break;
                        case 10:
                            levelItemLocations[x, y] = 4;
                            levelObjectLocations[x, y] = 2;
                            break;
                        case 11:
                            levelObjectLocations[x, y] = 6;
                            break;
                        case 12:
                            levelObjectLocations[x, y] = 7;
                            break;
                        case 13:
                            levelItemLocations[x, y] = 7;
                            levelObjectLocations[x, y] = 2;
                            break;
                        case 14:
                            levelObjectLocations[x, y] = 8;
                            break;
                        case 15:
                            levelItemLocations[x, y] = 8;
                            levelObjectLocations[x, y] = 2;
                            break;
                        default:
                            break;
                    }
                }
            }
            #region levelgen

            #region objectsingrid
            for (int x = 0; x < levelObjectLocations.GetLength(0); x++)
            {
                for (int y = 0; y < levelObjectLocations.GetLength(1); y++)
                {
                    switch (levelObjectLocations[x, y])
                    {
                        case 1:
                            //Debug.Log(x + "," + y + " has Wall");
                            GameObject wall = Instantiate(wallPrefab, levelGrid[x, y].PosGrid, Quaternion.identity);
                            levelGrid[x, y].objectInGrid = wall;
                            wall.GetComponent<Base_Object>().levelObj = this;
                            levelGrid[x, y].AcceptObject = false;
                            NetworkServer.Spawn(wall);
                            break;
                        case 2:
                            //Debug.Log(x + "," + y + " has Floor");
                            GameObject floor = Instantiate(floorPrefab, levelGrid[x, y].PosGrid, Quaternion.identity);
                            levelGrid[x, y].objectInGrid = floor;
                            floor.GetComponent<Base_Object>().levelObj = this;
                            NetworkServer.Spawn(floor);
                            break;
                        case 3:// Doors
                            {

                                if (levelGrid[x, y].AcceptObject)
                                {
                                    var item = (GameObject)Instantiate(
                                        ItemPrefab[2],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                    item.GetComponent<Base_Object>().levelObj = this;
                                    // to test power system
                                    if (x == 12)
                                    {
                                        item.GetComponent<Door>().powerOn = false;
                                    }
                                    levelGrid[x, y].objectInGrid = item;
                                    NetworkServer.Spawn(item);
                                }
                            }

                            break;
                        case 4:// power gen
                            {
                                if (levelGrid[x, y].AcceptObject)
                                {
                                    var item = (GameObject)Instantiate(
                                        ItemPrefab[3],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                    item.GetComponent<Base_Object>().levelObj = this;
                                    item.GetComponent<Power_Generator>().isSlave = true;
                                    item.GetComponent<Power_Generator>().grid = ClosestGrid(item.transform.position);
                                    levelGrid[x, y].objectInGrid = item;
                                    SlaveGens.Add(item);
                                    item.name = "Slave Gen";
                                    NetworkServer.Spawn(item);
                                }
                            }
                            break;

                        case 5:// power gen
                            {
                                if (levelGrid[x, y].AcceptObject)
                                {
                                    GameObject item = (GameObject)Instantiate(
                                        ItemPrefab[3],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                    item.GetComponent<Base_Object>().levelObj = this;
                                    item.GetComponent<Power_Generator>().isSlave = false;
                                    item.GetComponent<Power_Generator>().grid = ClosestGrid(item.transform.position);
                                    levelGrid[x, y].objectInGrid = item;
                                    MainGen = item;
                                    NetworkServer.Spawn(item);
                                }
                            }
                            break;
                        case 6:// flux spawner
                            {
                                var item = (GameObject)Instantiate(
                                        ItemPrefab[8],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;
                                levelGrid[x, y].objectInGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;
                        case 7:// plate spawner
                            {
                                var item = (GameObject)Instantiate(
                                        ItemPrefab[9],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;
                                levelGrid[x, y].objectInGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;
                        case 8:// circle spawner
                            {
                                var item = (GameObject)Instantiate(
                                        ItemPrefab[10],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;
                                levelGrid[x, y].objectInGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;

                        default:
                            break;
                    }
                    #endregion
                    //----
                    switch (levelItemLocations[x, y])
                    {
                        case 1: // Item
                            //Debug.Log(x + "," + y + " has Wall");
                            if (levelGrid[x, y].AcceptObject)
                            {
                                var item = (GameObject)Instantiate(
                                    ItemPrefab[0],
                                    new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                    Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;
                                levelGrid[x, y].objectOnGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;
                        case 2: //tears
                            {
                                var item = (GameObject)Instantiate(
                                    ItemPrefab[1],
                                    new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                    Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;
                                levelGrid[x, y].objectOnGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;

                        case 3:// weilder
                            {

                                if (levelGrid[x, y].AcceptObject)
                                {
                                    var item = (GameObject)Instantiate(
                                        ItemPrefab[4],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                    item.GetComponent<Base_Object>().levelObj = this;

                                    levelGrid[x, y].objectOnGrid = item;
                                    NetworkServer.Spawn(item);
                                }
                            }
                            break;
                        case 4:// Grinder
                            {

                                if (levelGrid[x, y].AcceptObject)
                                {
                                    var item = (GameObject)Instantiate(
                                        ItemPrefab[5],
                                        new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                        Quaternion.identity);
                                    item.GetComponent<Base_Object>().levelObj = this;
                                    levelGrid[x, y].objectOnGrid = item;
                                    NetworkServer.Spawn(item);
                                }
                            }
                            break;
                        case 5:// flux

                            break;
                        case 6:// plates
                            break;
                        case 7:// scanner
                            if (levelGrid[x, y].AcceptObject)
                            {
                                var item = (GameObject)Instantiate(
                                    ItemPrefab[7],
                                    new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                    Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;

                                levelGrid[x, y].objectOnGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;
                        case 8:// splicer
                            if (levelGrid[x, y].AcceptObject)
                            {
                                var item = (GameObject)Instantiate(
                                    ItemPrefab[11],
                                    new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - 1f),
                                    Quaternion.identity);
                                item.GetComponent<Base_Object>().levelObj = this;

                                levelGrid[x, y].objectOnGrid = item;
                                NetworkServer.Spawn(item);
                            }
                            break;
                        default:
                            break;
                    }
                    //----//----//----
                    switch (levelUnderLocations[x, y])
                    {
                        case 1:
                            {// cables
                                var obj = (GameObject)Instantiate(
                                underPrefabs[0],
                                new Vector3(levelGrid[x, y].PosGrid.x, levelGrid[x, y].PosGrid.y, levelGrid[x, y].PosGrid.z - .5f),
                                Quaternion.identity);
                                obj.GetComponent<Cable>().levelObj = this;
                                levelGrid[x, y].objectUnderGrid = obj;
                                obj.GetComponent<Cable>().grid = levelGrid[x, y];
                                //NetworkServer.Spawn(obj);
                                cableList.Add(obj);
                                break;
                            }
                        default:
                            break;

                    }
                    // Initlize level components

                }
            }
            #endregion

            if (cableList.Count >= cablestobreak)
            {
                int i = 0;
                while (i < cablestobreak)
                {
                    int tempint = Random.Range(0, cableList.Count - 1);
                    if (cableList[tempint].GetComponent<Cable>().connected == true)
                    {
                        if (cableList[tempint].GetComponent<Cable>().grid != null && (cableList[tempint].GetComponent<Cable>().grid.objectOnGrid == null || cableList[tempint].GetComponent<Cable>().grid.objectOnGrid.GetComponent<Powered>() != null))
                        {
                            cableList[tempint].GetComponent<Cable>().ChangeConnect();
                            i++;
                        }
                    }
                }

            }
            if (MainGen != null)
            {
                //give main gen shape array
                MainGen.GetComponent<Power_Generator>().SetShapes();
                foreach (GameObject g in SlaveGens)
                {
                    MainGen.GetComponent<Power_Generator>().AddSlaveBlock(g);

                }

                MainGen.GetComponent<Power_Generator>().SetPuzzle(genShapesMissing);
            }
        }
        startTimer();
    }
	// Update is called once per frame
	void Update () {
        Timer();
	}
    public Grid  ClosestGrid(Vector3 pos)
    {
        pos = Camera.main.WorldToScreenPoint(pos);
        float x = pos.x;
        float y = pos.y;
        // push numbers to non negative and remove offset to not interfere with modulus

        pos.x += - gridOffset.x - gridSpacing / 2;
        pos.y += - gridOffset.y - gridSpacing / 2;

        // get remainder and grid number

        float xmod = pos.x % gridSpacing;
        float ymod = pos.y % gridSpacing;
        //get grid number
        x = (pos.x - xmod) / gridSpacing;
        y = (pos.y - ymod) / gridSpacing;

        if (xmod > (gridSpacing / 2 ))
        {
            //Debug.Log(xmod +" xmod");
            x += 1;
        }
        if (ymod > (gridSpacing / 2 ))
        {
            y += 1;
        }
        return levelGrid[(int)x, (int)y];
    }

    public Grid[] GetNieghbourGrids(Grid centerGrid) {
        //x besides
        List<Grid> grids = new List<Grid>();
        for (int i = -1; i <= 1;i++)
        {
            if ((int)centerGrid.NumGrid.x + i >= 0 && (int)centerGrid.NumGrid.x + i < gridSize.x)
            {
                if (levelGrid[(int)centerGrid.NumGrid.x + i, (int)centerGrid.NumGrid.y].NumGrid != centerGrid.NumGrid)
                {
                    grids.Add(levelGrid[(int)centerGrid.NumGrid.x + i, (int)centerGrid.NumGrid.y]);
                }
                //if within bounds
                //if not given grid
                //add to list
            }
        }
        //y besides
        for (int i =  -1; i <=1; i++)
        {
            if ((int)centerGrid.NumGrid.y + i >= 0 && (int)centerGrid.NumGrid.y + i < gridSize.y)
            {
                if (levelGrid[(int)centerGrid.NumGrid.x, (int)centerGrid.NumGrid.y + i].NumGrid != centerGrid.NumGrid)
                {

                    grids.Add(levelGrid[(int)centerGrid.NumGrid.x, (int)centerGrid.NumGrid.y + i]);
                }
            }
            //if within bounds
            //if not given grid
            //add to list
        }
        return grids.ToArray(); ;
    }

    /// <summary>
    /// Checks if the block will support an overweild
    /// </summary>
    /// <param name="target">the grid to check</param>
    /// <returns> if the block now has an overweild return true, else return false</returns>
    public bool TryOverweildOnTarget( Grid target)
    {
        if(target.objectOnGrid == null)
        {
            var item = (GameObject)Instantiate(
                                    ItemPrefab[6],
                                    new Vector3(target.PosGrid.x, target.PosGrid.y, target.PosGrid.z - 1f),
                                    Quaternion.identity);
            item.GetComponent<Base_Object>().levelObj = this;
            target.objectOnGrid = item;
            NetworkServer.Spawn(item);
            return true;
        }

        return false;
    }

    void startTimer()
    {
        startTime = Time.time;
    }

    void Timer()
    {
    }
}

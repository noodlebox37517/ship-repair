﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scannable : MonoBehaviour {
    float timeout;
    public bool show = false;
    public bool scannable = true;
	// Use this for initialization
	void Start () {
        if (scannable && !show) {
            this.GetComponent<SpriteRenderer>().enabled = false;
        }
        timeout = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        if (show && scannable)
        {
            if (Time.time >= timeout)
            {
                show = false;
                this.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
	}
    public void Scanned(float timer)
    {
        if (scannable)
        {
            Debug.Log("scannable set to show");
            show = true;
            timeout = Time.time + timer;
            this.GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}

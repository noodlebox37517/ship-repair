﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Ground_Object : Base_Object{
    public enum GroundType
    {
        HullTear,
        Door,
        None,
        floor,
        spawner
    }
    [SyncVar]
    public GroundType TypeOf;
    [SyncVar]
    protected ToolTimers times = new ToolTimers();
    protected float defualtTime = 2.0f;
    [SyncVar]
    private bool _hasPlate = false;
    protected bool HasPlate
    {
        get
        {
            return _hasPlate;
        }
        set
        {
            _hasPlate = value;
            RpcupdateGraphic(IsGrinded,_hasPlate);
            //EventChangeGraphic();
        }
    }
    [SyncVar]
    private bool _isGrinded = false;
    public bool IsGrinded
    {
        get
        {
            return _isGrinded;
        }
        set
        {
            _isGrinded = value;
            if (_isGrinded)
            {
                levelObj.GetComponent<LevelState>().GrindedTiles += 1;
                if (this.grid.objectUnderGrid != null)
                {
                    if (this.grid.objectUnderGrid.GetComponent<Scannable>() != null)
                    {
                        this.grid.objectUnderGrid.GetComponent<Scannable>().scannable = false;
                        this.grid.objectUnderGrid.GetComponent<SpriteRenderer>().enabled = true;
                    }
                }
            }
            else
            {
                levelObj.GetComponent<LevelState>().GrindedTiles -= 1;
                if (this.grid.objectUnderGrid != null)
                {
                    if (this.grid.objectUnderGrid.GetComponent<Scannable>() != null)
                    {
                        this.grid.objectUnderGrid.GetComponent<Scannable>().scannable = true;
                        this.grid.objectUnderGrid.GetComponent<SpriteRenderer>().enabled = false;
                    }
                }
            }
            //RpcupdateProgress();
            RpcupdateGraphic(_isGrinded, HasPlate);
            //EventChangeGraphic();
        }
    }
    
    public Sprite defaultSprite;
    public Sprite PlateSprite;
    public Sprite GrindedSprite;
    //public delegate void ChangeGraphicRequired();
    //[SyncEvent]
    //public event ChangeGraphicRequired EventChangeGraphic;
    [SyncVar]
    protected float lastTime = 0;
    [SyncVar]
    protected Tool.ToolType lastTool = Tool.ToolType.None;
    [SyncVar]
    protected float timeRequiredRemaining = 2;
    [SyncVar]
    protected float timeRequired = 2;

    public float ProgressDecayDelay = 3;
    public float progressDecayPerTick = 0.5f;

    [Header("ProgressSetup")]
    public Slider p_slider;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        TypeOf = GroundType.None;
        DefaultGridHeight = Grid.GridHeight.On;
        timeRequiredRemaining = timeRequired;
        if (defaultSprite == null)
        {
            defaultSprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
        }
        CmdupdateProgress();
        lastTime = Time.time;

    }

    public override void OnStartServer()
    {
        //PlaceOnGrid();
    }
    protected virtual void Update()
    {
        if ((Time.time - lastTime) > ProgressDecayDelay && timeRequiredRemaining < timeRequired)
        {
            Debug.Log("tick" + this.name);
            CmdupdateProgress();
        }

    }
    [Command]
    protected void CmdupdateProgress()
    {

        timeRequiredRemaining = Mathf.Min(timeRequiredRemaining + (Time.deltaTime * progressDecayPerTick), timeRequired);
        RpcupdateProgress((timeRequiredRemaining / timeRequired) * 100);
    }
    #region support Functions
    /// <summary>
    /// this function will update the part bools,
    /// when overriding setup so all are set to false before calling base, then check and return if true
    /// </summary>
    [Command]
    public override void CmdUpdateGrid()
    {
        base.CmdUpdateGrid();

        HasPlate = false;
        Grid tempGrid = levelObj.ClosestGrid(this.transform.position);
        if (tempGrid.objectOnGrid != null && tempGrid.objectOnGrid.GetComponent<Part>() != null)
        {
            if (IsGrinded == true && tempGrid.objectOnGrid.GetComponent<Part>().TypeOf == Part.PartType.Plate)
            {
                HasPlate = true;
                return;
            }
        }
    }
    [ClientRpc]
    public virtual void RpcupdateGraphic(bool newIsGrinded, bool newHasPlate)
    {
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        if (newIsGrinded && !newHasPlate)
        {
            if (GrindedSprite != null)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = GrindedSprite;
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
        else if (newHasPlate)
        {
            if (PlateSprite != null)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = PlateSprite;
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = defaultSprite;
        }
        
    }

    #endregion

    #region ToolCommands


    /// <summary>
    /// This function is used to enable Tool usage
    /// </summary>
    [Command]
    public virtual void CmdUseToolOn(Tool.ToolType ToolUsed)
    {
        lastTime = Time.time;
        //Calulate time remaining
        //if (Time.deltaTime == (lastTime - Time.fixedTime))
        if (ToolUsed == lastTool && ToolRequirements(ToolUsed))
        {
            timeRequiredRemaining -= Time.deltaTime;
        }else if(ToolUsed != lastTool)
        {
            if(!times.GetTime(ToolUsed, out timeRequired))
            {
                Debug.Log("defualt Time used (Tool: " + ToolUsed);
                timeRequiredRemaining = defualtTime;
            }
            lastTool = ToolUsed;
            timeRequiredRemaining = timeRequired;
        }
        
        //call action
        if (timeRequiredRemaining <= 0)
        {
            switch (ToolUsed)
            {
                case Tool.ToolType.Weilder:
                    Debug.Log("I've being weilded");
                    CmdWeild();
                    break;
                case Tool.ToolType.Grinder:
                    Debug.Log("They've ground me down scotty");
                    CmdGrind();
                    break;
                case Tool.ToolType.Scanner:
                    break;
                case Tool.ToolType.Splicer:
                    //    //do nothing as splicer doesnt work on this block
                    CmdSplice();
                    break;
                case Tool.ToolType.None:
                    throw new System.Exception("Unknow Tool type:None");
                    //break;
                default:
                    break;
            }
            //reset time
            if (!times.GetTime(ToolUsed, out timeRequired))
            {
                timeRequired = defualtTime;
            }
            timeRequiredRemaining = timeRequired;
        }
        CmdupdateProgress();
    }
    [Command]
    private void CmdSplice()
    {
        if (grid.objectUnderGrid.GetComponent<Cable>() != null)
        {
            grid.objectUnderGrid.GetComponent<Cable>().ChangeConnect();
        }
    }

    protected virtual bool ToolRequirements(Tool.ToolType toolUsed)
    {
        switch (toolUsed)
        {
            case Tool.ToolType.Weilder:
                //if we have the plate or isnt ground down
                if(HasPlate == true || !IsGrinded)
                {
                    return true;
                }
                break;
            case Tool.ToolType.Grinder:
                //if not already groundDown
                if (!IsGrinded)
                {
                    return true;
                }
                break;
            case Tool.ToolType.Scanner:

                break;
            case Tool.ToolType.Splicer:
                if (IsGrinded == true && grid.objectInGrid != null && grid.objectUnderGrid.GetComponent<Cable>() != null)
                {
                    return true;
                }
                break;
            case Tool.ToolType.None:

                break;
            default:
                return false;
        }
        return false;
    }

    /// <summary>
    /// Sees if the ground_Object can use the part, if it uses the part return true.
    /// </summary>
    /// <param name="partUsed">the type of part used on the block</param>
    /// <returns>return true if the </returns>
    public virtual bool UsePartOn(Part.PartType partUsed)
    {
        if(IsGrinded && !HasPlate && partUsed == Part.PartType.Plate)
        {
            CmdUpdatePart(partUsed);
            
            return true;
        }

        return false;
    }
    [Command]
    public virtual void CmdUpdatePart(Part.PartType partUsed)
    {
        if (partUsed == Part.PartType.Plate)
        {
            HasPlate = true;
            RpcupdateGraphic(_isGrinded, _hasPlate);
        }
    }


    [ClientRpc]
    private void RpcupdateProgress(float timePercent)
    {
        //get percent done (100 - percent as it fills as complete)
        p_slider.value = 100 - timePercent;
    }
    
    [Command]
    public  virtual void CmdGrind()
    {
        //Code to grind down block
        //this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        IsGrinded = true;
        RpcupdateGraphic(IsGrinded,HasPlate);
    }

    [Command]
    public virtual void CmdWeild()
    {
        //check if sprite renderer is on or not
        Debug.Log("defualt weild done");
        if (IsGrinded && HasPlate)
        {
            IsGrinded = false;
            HasPlate = false;
        }else
        {
            //code to add overWeild
            Grid targrid = levelObj.ClosestGrid(this.transform.position);
            levelObj.TryOverweildOnTarget(targrid);
        }
    }
    #endregion

}

public class ToolTimers
{
    private Dictionary<Tool.ToolType, float> ToolTimes = new Dictionary<Tool.ToolType, float>();

    public ToolTimers()
    {
        //Need to find a better way/place to do this, anyhow....
        ToolTimes.Add(Tool.ToolType.None, 100);
        ToolTimes.Add(Tool.ToolType.Grinder, 1.5f);
        ToolTimes.Add(Tool.ToolType.Weilder, 1.0f);
        ToolTimes.Add(Tool.ToolType.Scanner, 0.5f);
        ToolTimes.Add(Tool.ToolType.Splicer, 2.0f);
    }

    public bool GetTime(Tool.ToolType tool, out float time )
    {
        return ToolTimes.TryGetValue(tool, out time);
    }

    public void UpdateTime(Tool.ToolType tool, float time){
        //check if updating or adding
        if (ToolTimes.ContainsKey(tool))
        {
            ToolTimes[tool] = time;
        }else
        {
            ToolTimes.Add(tool, time);
        }
    }
}

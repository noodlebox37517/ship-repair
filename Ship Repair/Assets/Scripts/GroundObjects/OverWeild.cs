﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverWeild : Ground_Object {
    protected override void Start()
    {
        base.Start();
        TypeOf = GroundType.HullTear;
        levelObj.GetComponent<LevelState>().Overwelds += 1;
    }
    protected override void Update()
    {
        base.Update();
    }
    //public override void UseToolOn(Tool.ToolType ToolUsed)
    //{
    //    base.UseToolOn(ToolUsed);

    //    if(ToolUsed == Tool.ToolType.Weilder)
    //    {
    //        if (!times.GetTime(ToolUsed, out timeRequiredRemaining))
    //        {
    //            Debug.Log("defualt Time used (Tool: " + ToolUsed);
    //            timeRequiredRemaining = defualtTime;
    //        }
    //    }
    //}

    protected override bool ToolRequirements(Tool.ToolType toolUsed)
    { 
        if(toolUsed == Tool.ToolType.Weilder)
        {
            return false;
        }

        return base.ToolRequirements(toolUsed);
    }

    //[Command]
    public override void CmdWeild()
    {
        //not much happens
    }
    //[Command]
    public override void CmdGrind()
    {
        //removed the overweild
        Debug.Log("Destorying overweild");
        levelObj.GetComponent<LevelState>().Overwelds -= 1;
        GameObject.Destroy(this.gameObject);
    }
}

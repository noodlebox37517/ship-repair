﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Hull_Tear : Ground_Object
{
    private bool fluxed = false;
    public Sprite fluxedSprite;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        TypeOf = GroundType.HullTear;
        times.UpdateTime(Tool.ToolType.Weilder, 2);
        //add to lsit of problems
        levelObj.GetComponent<LevelState>().tearsLeft += 1;
    }
    protected override void Update()
    {
        base.Update();
    }
    public override bool UsePartOn(Part.PartType partUsed)
    {        
        //if not fluxed use the flux up
        if(partUsed == Part.PartType.Flux && fluxed == false)
        {
            fluxed = true;
            return true;
        }
        return false;
    }

    public override void RpcupdateGraphic(bool newIsGrinded, bool newHasPlate)
    {
        base.RpcupdateGraphic( newIsGrinded, newHasPlate);
        if (fluxed)
        {
            if (fluxedSprite != null)
            {
                grid.objectInGrid.GetComponent<SpriteRenderer>().sprite = fluxedSprite;
                return;
            }
        }
        grid.objectInGrid.GetComponent<SpriteRenderer>().sprite = defaultSprite;
    }

    /// <summary>
    /// this function will update the part bools,
    /// when overriding setup so all are set to false before calling base, then check and return if true
    /// </summary>
    public override void CmdUpdateGrid()
    {
        fluxed = false;
        base.CmdUpdateGrid();
        if (grid.objectOnGrid.GetComponent<Part>() != null)
        {
            if (this.gameObject.GetComponent<SpriteRenderer>().enabled == false && grid.objectOnGrid.GetComponent<Part>().TypeOf == Part.PartType.Flux)
            {
                fluxed = true;
                return;
            }
        }
    }

    protected override bool ToolRequirements(Tool.ToolType toolUsed)
    {
        //do this check before standard checks as it takes presidence
        if(toolUsed == Tool.ToolType.Weilder)
        {
            if (fluxed)
            {
                return true;
            }else
            {
                return false;
            }
        }
        return base.ToolRequirements(toolUsed);
    }

    //[Command]
    public override void CmdWeild()
    {
        if (fluxed)
        {
            //removed the hull tear
            Debug.Log("Destorying HullTear");
            //Grid targrid = levelObj.ClosestGrid(this.gameObject.transform.position);
            grid.objectOnGrid = null;
            levelObj.GetComponent<LevelState>().tearsLeft += -1;
            GameObject.Destroy(this.gameObject);
        }
    }
    //[Command]
    public override void CmdGrind()
    {
        //Grid targrid = levelObj.ClosestGrid(this.gameObject.transform.position);
        //hide the floor underneath
        grid.objectInGrid.GetComponent<Ground_Object>().IsGrinded = true;
        //remove the hull tear
        grid.objectOnGrid = null;
        GameObject.Destroy(this.gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : Ground_Object {
    
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        TypeOf = GroundType.floor;
    }
    protected override void Update()
    {
        base.Update();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map_Generator : MonoBehaviour {
    public Vector2 gridSize = new Vector2(28, 16);
    public Color testColor;
    public Color[] generationValues;
    public Texture2D imageSource;
    public int[] mapLayout;

    // Use this for initialization
    void Start () {
        int[,] levelItemLocations;
        levelItemLocations = new int[(int)(gridSize.x), (int)(gridSize.y)];
        mapGeneration(ref levelItemLocations);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void mapGeneration(ref int[,] mapOutArray)
    {
        // loads the colour data of each pixel into an array

        Color[] pixelColours = imageSource.GetPixels();
        mapLayout = new int[pixelColours.Length];
        // works out the conversion to the correct tiletype (as defined in level controller) then loads that into another array to be used to build the level
        for (int i = 0; i<pixelColours.Length; i++)
        {
            pixelColours.ToString();
            if (pixelColours[i] == generationValues[0])
            {//walls plain
                mapLayout[i] = 1;
            }
            else if (pixelColours[i] == generationValues[1] || pixelColours[i] == generationValues[2])
            {//floors no cable
                mapLayout[i] = 2;
            }
            else if (pixelColours[i] == generationValues[3])
            {//doors has cable
                mapLayout[i] = 3;
            }
            else if (pixelColours[i] == generationValues[4])
            {//floor with cable
                mapLayout[i] = 4;
            }
            else if (pixelColours[i] == generationValues[5])
            {//powergen slave 
                mapLayout[i] = 5;
            }
            else if (pixelColours[i] == generationValues[6])
            {//master pwoer gen
                mapLayout[i] = 6;
            }
            else if (pixelColours[i] == generationValues[7])
            {//Hull_Tear no cable
                mapLayout[i] = 7;
            }
            else if (pixelColours[i] == generationValues[8])
            {//hull tear cabled
                mapLayout[i] = 8;
            }
            else if (pixelColours[i] == generationValues[9])
            {//tool
                mapLayout[i] = 9;
            }
            else if (pixelColours[i] == generationValues[10])
            {//tool
                mapLayout[i] = 10;
            }
            else if (pixelColours[i] == generationValues[11])
            {//flux
                mapLayout[i] = 11;
            }
            else if (pixelColours[i] == generationValues[12])
            {//part plate
                mapLayout[i] = 12;
            }
            else if (pixelColours[i] == generationValues[13])
            {//tool
                mapLayout[i] = 13;
            }
            else if (pixelColours[i] == generationValues[14])
            {//circle
                mapLayout[i] = 14;
            }
            else if (pixelColours[i] == generationValues[15])
            {//circle
                mapLayout[i] = 15;
            }
            else
            {
                mapLayout[i] = -1;
            }
                

        }

        // Now convert that array to a Grid for compatibility with Level_Controller.cs
        // Convert array by writing each row individually. Gridsize 28, 16
        int m = 0;
        for (int gridY = 0; gridY < gridSize.y; gridY++)
        {
            for (int gridX = 0; gridX < gridSize.x; gridX++)
        {
                mapOutArray[gridX, gridY] = mapLayout[m];
                m++;
            }
        }
    }
}

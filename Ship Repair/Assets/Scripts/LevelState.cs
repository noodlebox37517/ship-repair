﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelState : MonoBehaviour {
    public  int _objectsWithoutPower = 0;
    public int objectsWithoutPower
    {
        get
        {
            return _objectsWithoutPower;
        }
        set
        {
            _objectsWithoutPower = value;
            if (_objectsWithoutPower<0)
            {
                _objectsWithoutPower = 0;
            }
        }
    }
    public int cablesDamaged = 0;
    public int tearsLeft =0;
    public int Overwelds = 0;
    public int GrindedTiles = 0;
    public int ProblemsLeft = 0;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        ProblemsLeft = objectsWithoutPower + cablesDamaged + tearsLeft + Overwelds + GrindedTiles;
        if (ProblemsLeft < 0)
        {
            Debug.Log("YOU WIN!");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : Part {

	// Use this for initialization
	protected override void Start () {
        base.Start();
        TypeOf = PartType.Plate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override bool place(Grid targetGrid)
    {
        return base.place(targetGrid);
    }
}

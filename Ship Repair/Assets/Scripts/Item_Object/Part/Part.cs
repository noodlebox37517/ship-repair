﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : Item_Object
{

    public enum PartType
    {
        Cable,
        Plate,
        Flux,
        None
    }

    public PartType TypeOf = PartType.None;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        //PlaceGrid();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual bool place(Grid targetGrid)
    {
        Ground_Object target;
        if (targetGrid.objectOnGrid != null)
        {
            //try to apply this tool to the grid
            target = targetGrid.objectOnGrid.GetComponent<Ground_Object>();
            if (target != null)
            {
                return target.UsePartOn(TypeOf);
            }
        }
        target = targetGrid.objectInGrid.GetComponent<Ground_Object>();
        if (target != null)
        {
            return target.UsePartOn(TypeOf);
        }
        return false;
    }

}

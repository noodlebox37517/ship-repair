﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : Part {
    public int shapeID;
    // number of acceptable adjacents
    public int[] numedges;
    public int[] acceptableAdjacentShapeIDs;
    public bool limited =false;
    public int limitedto = 0;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        // if is not above a power gen scannable = false

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SetShape(int id, int[] edges,int[] adjacentshapes, bool limit,int limitnum)
    {
        shapeID = id;
        
        numedges = edges;
        if (limit)
        {
            limited = limit;
            limitedto = limitnum;
            acceptableAdjacentShapeIDs = adjacentshapes;
        }
    }
    public void SetShape(int id, int[] edges, int[] adjacentshapes)
    {
        shapeID = id;
        numedges = edges;
        acceptableAdjacentShapeIDs = adjacentshapes;
    }
    public void GeneratorProcedure()
    {
        if(grid.objectInGrid.GetComponent<Power_Generator>()!= null)
        {
            grid.objectInGrid.GetComponent<Power_Generator>().shapeObj = this.gameObject;
        }
    } 
    public bool CheckCompatability(int adjacentnum, int[] adjacentshapesids)
    {
        bool contcheck = false;
        foreach (int e in numedges) {
            if (adjacentnum == e)
            {
                contcheck = true;
            }
        }
        if (!contcheck)
        {
            return false;
        }
        contcheck = false;
        if (adjacentshapesids.Length !=0)
        {
        foreach (int a in adjacentshapesids)
            {
                foreach (int b in acceptableAdjacentShapeIDs)
                {
                    if (a == b)
                    {
                        contcheck = true;
                    }
                }

            }
        }
        else
        {
            contcheck = true;
        }
        if (!contcheck)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public override void Placed(GameObject placer)
    {
        base.Placed(placer);
        if (grid.objectInGrid != null && grid.objectInGrid.GetComponent<Power_Generator>()!= null)
        {
            grid.objectInGrid.GetComponent<Power_Generator>().currShape = this;
            this.GetComponent<Scannable>().scannable = true;
            grid.objectInGrid.GetComponent<Power_Generator>().masterGen.GetComponent<Power_Generator>().TurnGen(true);
        }
        // check power gen is in grid
        // give shape to gen
        //make invisible
        //Set to scannable
        // 
    }
  
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weilder : Tool {
    protected override void Start () {
        base.Start();
        TypeOf = ToolType.Weilder;
	}

    public override void use(Grid targetGrid)
    {
        base.use(targetGrid);

        //any side effects of using the tool go here...
    }
}

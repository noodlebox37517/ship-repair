﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scanner : Tool{
    public int scanRange = 1;
    public float scanTime = 3f;
	// Use this for initialization
	void Start () {
        coolDown = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public override void use(Grid targetGrid)
    {
       Grid centerGrid = levelObj.ClosestGrid(this.transform.position);       //get tool position                                                                     //get closets grid
        List<Grid> grids = new List<Grid>();
        Vector3 gridSize = levelObj.gridSize;
        Grid[,] levelGrid = levelObj.levelGrid;
        for (int x = -scanRange; x <= scanRange; x++)
        {
            for (int y = -scanRange; y <= scanRange; y++)
            {
                if ((int)centerGrid.NumGrid.x + x >= 0 && (int)centerGrid.NumGrid.x + x < gridSize.x && (int)centerGrid.NumGrid.y + y >= 0 && (int)centerGrid.NumGrid.y + y < gridSize.y)
                {
                    if (levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].NumGrid != centerGrid.NumGrid)
                    {
                        // check each object for scanabble script
                       if( levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectOnGrid!= null && levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectOnGrid.GetComponent<Scannable>() != null)
                        {
                            levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectOnGrid.GetComponent<Scannable>().Scanned(scanTime) ;
                        }
                        if (levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectInGrid != null && levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectInGrid.GetComponent<Scannable>() != null)
                        {
                            levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectInGrid.GetComponent<Scannable>().Scanned(scanTime);
                        }
                        if (levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectUnderGrid != null && levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectUnderGrid.GetComponent<Scannable>() != null)
                        {
                            levelGrid[(int)centerGrid.NumGrid.x + x, (int)centerGrid.NumGrid.y + y].objectUnderGrid.GetComponent<Scannable>().Scanned(scanTime);
                        }
                    }
                }

            }
        }
           
               
            //find all gids in range , add to array if scannable and turn scanned on
            //

            //off does off signal work

            //any side effects of using the tool go here...
        }
}

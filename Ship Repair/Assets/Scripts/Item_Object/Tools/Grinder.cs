﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grinder : Tool {

    // Use this for initialization

    int grinderDamage = 5;
    bool playerInRange = false;
    GameObject player;
    List <GameObject> playersInRange = new List<GameObject>();

       protected override void Start() {
        base.Start();
        TypeOf = ToolType.Grinder;
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
	}

    public override void use(Grid targetGrid)
    {
        base.use(targetGrid);

        if (playersInRange.Count > 0)
        { 
            foreach(GameObject g in playersInRange)
            {
                damagePlayer(g);
            }
        
        }
        //any side effects of using the tool go here...

        //grinder needs to have a movement effect added here

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playersInRange.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playersInRange.Remove(other.gameObject);
        }
    }

    void damagePlayer(GameObject player)
    {
        if (playerInRange == true)
        {
            player.GetComponent<Player_Controller>().playerTakeDamage(10);
        }
    }
}

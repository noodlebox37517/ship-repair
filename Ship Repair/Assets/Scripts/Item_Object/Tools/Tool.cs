﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : Item_Object {

	public enum ToolType
	{
		Weilder,
		Grinder,
		Scanner,
		Splicer,
		None
	}

	public ToolType TypeOf = ToolType.None;
    public float coolDown = 0;
	protected override void Start () {
		base.Start();
		TypeOf = ToolType.None;
	}

	public override void use(Grid targetGrid)
	{
		{
			Ground_Object target;
			if (targetGrid.objectOnGrid != null) {
				//try to apply this tool to the grid
				 target = targetGrid.objectOnGrid.GetComponent<Ground_Object>();
				if (target != null)
				{
					target.CmdUseToolOn(TypeOf);
					return;
				}
			}
            if (targetGrid.objectInGrid != null)
            {
                target = targetGrid.objectInGrid.GetComponent<Ground_Object>();
                if (target != null)
                {
                    target.CmdUseToolOn(TypeOf);
                    return;
                }
            }
		}
	}

}

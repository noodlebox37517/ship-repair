﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid {

    public Vector2 NumGrid;
    public Vector3 PosGrid;
    public bool AcceptObject;
    public GameObject objectOnGrid;
    public GameObject objectUnderGrid;
    public GameObject objectInGrid;

    public enum GridHeight
    {
        under,
        In,
        On
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    /// <summary>
    /// sets values of grid
    /// </summary>
    /// <param name="num"> grid number</param>
    /// <param name="Pos"> position of grid </param>
    /// <param name="acceptsobjects"> sets grid to accept objects such as items and tools</param>
    public void SetValues(Vector2 num,Vector3 Pos,bool acceptsobjects)
    {
        NumGrid = num;
        PosGrid = Pos;
        AcceptObject = acceptsobjects;
    }

    public bool GiveObject(GameObject Gameobj)
    {

        if (objectOnGrid == null && AcceptObject)
        {
            objectOnGrid = Gameobj;
            Gameobj.GetComponent<Item_Object>().grid = this;
            Gameobj.transform.position = new Vector3(PosGrid.x, PosGrid.y, 1);
            Gameobj.transform.parent = null;
            //Debug.Log("Grid:" + PosGrid + "Got new item");
            //Tom "this wont assign if objectinGrid is null"
            Base_Object inGrid = null;

            if (objectInGrid != null) 
            inGrid = objectInGrid.GetComponent<Base_Object>();

            if(inGrid != null)
            {
                inGrid.CmdUpdateGrid();
            }
                return true;
            }
            return false;

    }
    public bool PlaceObject(GameObject Gameobj, GridHeight Height)
    {
        switch (Height)
        {
            case GridHeight.under:
                if (objectUnderGrid == null)
                {
                    objectUnderGrid = Gameobj;
                    //Debug.Log("Grid:" + PosGrid + "Got new item under");
                    return true;
                }
                break;
            case GridHeight.In:
                if (objectInGrid == null)
                {
                    objectInGrid = Gameobj;
                    //Debug.Log("Grid:" + PosGrid + "Got new item In");
                    return true;
                }
                break;
            case GridHeight.On:
                if (objectOnGrid == null)
                {
                    objectOnGrid = Gameobj;
                    //Debug.Log("Grid:" + PosGrid + "Got new item on");
                    return true;
                }
                break;
            default:
                break;
        }
        
        return false;

    }

    public bool TakeObject(out GameObject GameObj)
    {
        if(objectOnGrid != null)
        {
            GameObj = objectOnGrid;
            objectOnGrid = null; Base_Object inGrid = objectInGrid.GetComponent<Base_Object>();
            if (inGrid != null)
            {
                inGrid.CmdUpdateGrid();
            }
            //Debug.Log("Grid:" + PosGrid + "gave item");
            return true;
        }
        GameObj = null;
        return false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enviroment_Object : Base_Object{

	// Use this for initialization
	protected override void Start () {
        base.Start();
        DefaultGridHeight = Grid.GridHeight.In;

        if (this.GetComponent<Rigidbody2D>() == null)
        {
            gameObject.AddComponent<Rigidbody2D>();
        }
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;

        if(this.GetComponent<PolygonCollider2D>()== null)
        {
            gameObject.AddComponent<PolygonCollider2D>();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

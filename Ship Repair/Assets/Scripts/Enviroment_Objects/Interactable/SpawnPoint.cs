﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnPoint : Interactable
{
    public GameObject SpawnItem;
    public SpriteRenderer ChildSprite;

    protected override void Start()
    {
        base.Start();
        if(ChildSprite != null && SpawnItem != null)
        {
            ChildSprite.sprite = SpawnItem.GetComponent<SpriteRenderer>().sprite;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="gameObject"></param>
    public override void interact(ref GameObject gameObject)
    {
        base.interact(ref gameObject);
        //if the player isnt already holding somthing give them the newly spawned item
        if (gameObject == null)
        {
            //spawn object
            var item = (GameObject)Instantiate(
                SpawnItem,
                new Vector3(grid.PosGrid.x, grid.PosGrid.y, grid.PosGrid.z - 1f),
                Quaternion.identity);
            item.GetComponent<Item_Object>().levelObj = levelObj;
            grid.objectOnGrid = item;
            NetworkServer.Spawn(item);
            //assign the item to give back
            gameObject = item;
        }

    }



}

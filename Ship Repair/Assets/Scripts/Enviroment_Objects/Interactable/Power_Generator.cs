﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Power_Generator : Interactable
{
    public bool isSlave = true;
    public Cable connectedCable;
    public List<GameObject> powerGenSlaves = new List<GameObject>();
    public GameObject masterGen;
    //public Grid grid;

    [SyncVar]
    public bool givepower;
    [SyncVar]
    private int Edges = 4;
    public GameObject[] adjacentGenerators;
    public GameObject[] puzzleShapesobjects;
    private List<Shape> puzzleShapes = new List<Shape>();

    public Shape currShape;
    public GameObject shapeObj;
    // Use this for initialization 
    void Awake()
    {
        // grid = levelObj.GetComponent<Level_Controller>().ClosestGrid(this.transform.position);
    }
    protected override void Start()
    {
        base.Start();
        if (!isSlave)
        {
            masterGen = this.gameObject;
                if (connectedCable == null)
                {
                    FindCable();
                }
            TurnGen(true);
            // SetPuzzle();
        }
    }

    // Update is called once per frame
    void Update() {

    }
    void FixedUpdate()
    {
        if (!isSlave)
        {
            if (connectedCable == null)
            {
                FindCable();
            }
        }

    }
    public void SetShapes()
    {
        foreach (GameObject g in puzzleShapesobjects)
        {
            puzzleShapes.Add(g.GetComponent<Shape>());
        }
    }
    public void SetPuzzle(int shapestoremove)
    {
        if (!isSlave)
        {
            FindPositioninGenerators();
            if (SetGenShape(puzzleShapes))
            {
                Debug.Log("puzzle set");
                if(shapestoremove!=0)
                removeshapes(shapestoremove);
                return;
            }
            Debug.Log("puzzle failed");

        }
    }
    public void removeshapes(int num)
    {
        List<GameObject> templist;
        templist = new List<GameObject>(powerGenSlaves);
        templist.Add(this.gameObject);
        for(int i = 0; i < num; i++)
        {
           int objnum = Random.Range(0, templist.Count - 1);
            Destroy(templist[objnum].GetComponent<Power_Generator>().shapeObj);
            templist[objnum].GetComponent<Power_Generator>().currShape = null;
            templist.RemoveAt(objnum);
            //get rndm object
            //remove shape
            //remove object from templist
        }


    }
    public bool SetGenShape(List<Shape> possibleshapes)
    {
        //get possible shapes
        // get current adjacent shapes
        if (currShape != null)
        {
            return true;
        }

        List<Shape> acceptableshapes = new List<Shape>();
        List<int> adjacentshapes = new List<int>();

        foreach (GameObject g in adjacentGenerators)
        {
            if (g != null) {
                if (g.GetComponent<Power_Generator>().currShape != null) {
                    adjacentshapes.Add(g.GetComponent<Power_Generator>().currShape.shapeID);
                }
            }
        }

        foreach (Shape s in possibleshapes)
        {
            if (s.CheckCompatability(Edges, adjacentshapes.ToArray()))
            {
                acceptableshapes.Add(s);
            }
        }

        if (acceptableshapes.Count == 0)
        {
            return false;
        }
        else
        {
            bool shapeconfirmed = false;

            while (acceptableshapes.Count != 0 && !shapeconfirmed)
            {
               int shapenum = Random.Range(0, acceptableshapes.Count - 1);
                currShape = acceptableshapes[shapenum];

                foreach (GameObject g in adjacentGenerators)
                {
                    if (g != null)
                    {
                        if (g.GetComponent<Power_Generator>().SetGenShape(possibleshapes))
                        {
                            Debug.Log("Shape acceptable");
                            shapeconfirmed = true;
                        }
                        else
                        {
                            Debug.Log("Shape not acceptable");
                            currShape = null;
                            acceptableshapes.RemoveAt(shapenum);
                            break;
                            //Shape not acceptable
                        }
                    }
                }
                Debug.Log("end parse ");
            }
            if (currShape != null)
            {
                shapeObj = Instantiate(currShape.gameObject, this.transform.position, Quaternion.identity);
                shapeObj.GetComponent<Base_Object>().levelObj = levelObj;
                shapeObj.transform.position += new Vector3(0,0,-1) ;
                this.grid.AcceptObject = true;
                this.grid.objectOnGrid = shapeObj;
                currShape = shapeObj.GetComponent<Shape>();
                currShape.GetComponent<Scannable>().scannable = true;
                currShape.GetComponent<Item_Object>().pick_Upable = false;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
 
    /// <summary>
    /// Should be called to find cable for power connection to generator, the cable will be in a grid adjacent to the blocks only 1 cable will be conencted and stored
    /// ---should check for cables under anyway.---
    /// </summary>
    public void FindCable()
    {
        //checks non slave power blcok first
        Grid[] tocheck = levelObj.GetNieghbourGrids(grid);
        for (int i = 0; i <= tocheck.Length; i++)
        {
            if (tocheck[i].objectUnderGrid != null)
            {
                if(tocheck[i].objectUnderGrid.GetComponent<Cable>() != null)
                {
                    connectedCable = tocheck[i].objectUnderGrid.GetComponent<Cable>();
                    break;
                }
            }

        }
        //checks slaves if cable not set
        if (connectedCable == null)
        {
            foreach (GameObject slave in powerGenSlaves)
            {
                tocheck = levelObj.GetNieghbourGrids(slave.GetComponent<Power_Generator>().grid);
                for (int i = 0; i <= tocheck.Length; i++)
                {
                    if (tocheck[i].objectUnderGrid != null && tocheck[i].objectUnderGrid.GetComponent<Cable>() != null)
                    {
                        connectedCable = tocheck[i].objectUnderGrid.GetComponent<Cable>();
                        break;
                    }

                }
            }

        }
        if (connectedCable == null)
        {
            Debug.Log("generator did not find cable");
        }
    }
    /// <summary>
    /// add gameobject as slave to power gen, 0 will be returned if the target generator is not set to master
    /// </summary>
    /// <param name="slave"></param>
    /// <returns></returns>
    public void AddSlaveBlock(GameObject slave)
    {
        if (isSlave)
            return;
        slave.GetComponent<Power_Generator>().masterGen = this.gameObject;
        powerGenSlaves.Add(slave);
        slave.GetComponent<Power_Generator>().FindPositioninGenerators();

        //does the slave need to know what number it is?

    }
    /// <summary>
    /// Finds all generators in edge contact and sets edge contacts giving 
    /// 1= prong, 2= corner, 3 = midsection 4= surrounded. this is then inverted and stored in edgecontacts
    /// each contact is also stored in and ordered array 0= above,1=right,2=below,3 = left.
    /// </summary>
    public void FindPositioninGenerators()
    {

        Grid[] adjacentGrids = levelObj.GetNieghbourGrids(this.grid);

        GameObject[] nieghbour = new GameObject[4];
        int edgecontacts = 0;
        foreach (Grid g in adjacentGrids)
        {
            if (g.objectInGrid != null && g.objectInGrid.GetComponent<Power_Generator>() != null)
            {
                edgecontacts++;
                if (g.NumGrid.x - this.grid.NumGrid.x > 0)
                {
                    nieghbour[1] = g.objectInGrid;
                }
                else if (g.NumGrid.x - this.grid.NumGrid.x < 0)
                {
                    nieghbour[3] = g.objectInGrid;
                }
                if (g.NumGrid.y - this.grid.NumGrid.y > 0)
                {
                    nieghbour[0] = g.objectInGrid;
                }
                else if (g.NumGrid.y - this.grid.NumGrid.y < 0)
                {
                    nieghbour[2] = g.objectInGrid;
                }

            }
        }
        adjacentGenerators = nieghbour;

        // get what type it has and its actual position
        switch (edgecontacts)
        {
            case 0:
                {
                    Edges = 0; //alone

                }
                break;
            case 1:
                {
                    Edges = 1; //prong

                }
                break;
            case 2:
                {
                    Edges = 2;//corner
                }
                break;
            case 3:
                {
                    Edges = 3;// midesction
                }
                break;
            case 4:
                {
                    Edges = 4;//surrounded
                }
                break;
            default:
                break;
        }
    }
    public void TurnGen(bool on)
    {
        if (!on)
        {
            givepower = false;
            connectedCable.GetComponent<Power>().setpower(false);
            return;
        }
        bool working = true;
        foreach (GameObject g in powerGenSlaves)
        {
            if (g.GetComponent<Power_Generator>().currShape == null)
            {

                working = false;
                Debug.Log("currshape is null");
            }
           else if(g.GetComponent<Power_Generator>().CheckShape() == false)
            {
                working = false;
            }

        }
        if (currShape == null && CheckShape() == false)
            working = false;

        // check globals number of certain shapes or not specific requirements not iumplemented yet
        //
        //
        if (working == true)
        {
            givepower = on;
            connectedCable.GetComponent<Power>().setpower(on);
        }
        else
        {
            givepower = false;
            connectedCable.GetComponent<Power>().setpower(false);
        }

        //check parts
        // change state if correct
        //otherwise give error
    }
    public bool CheckShape()
    {
        bool returnval = false;
        if (currShape != null)
        {


            Shape shape = this.GetComponent<Shape>();
            List<int> adjacentshapes = new List<int>();

            foreach (GameObject g in adjacentGenerators)
            {
                if (g != null)
                {
                    if (g.GetComponent<Power_Generator>().currShape != null)
                    {
                        adjacentshapes.Add(g.GetComponent<Power_Generator>().currShape.shapeID);
                    }
                }





            }
            if (Edges >= 0 && adjacentshapes != null) {
                if (currShape.CheckCompatability(Edges, adjacentshapes.ToArray()))
                {
                    returnval = true;
                }
            }
        }

        return returnval;
    }
}

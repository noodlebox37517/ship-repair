﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powered : Interactable
{
    public bool _powerOn = false;
    public bool powerOn
    {
        get
        {
            return _powerOn;
        }
        set
        {
            _powerOn = value;
            if (_powerOn)
            {
                PowerUp();
                levelObj.GetComponent<LevelState>().objectsWithoutPower -= 1;
            }
            else
            {
                PowerDown();
                levelObj.GetComponent<LevelState>().objectsWithoutPower += 1;
            }
        }
    }
            // Use this for initialization
            void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public virtual void PowerUp()
    {

    }
    public virtual void PowerDown()
    {

    }
}

//needs to share power to objects on level
//needs to take power when looses power
// needs to take power and alert power giver when removed
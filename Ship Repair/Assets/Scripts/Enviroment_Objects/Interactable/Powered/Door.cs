﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Powered
{
    public bool openable = true;
    private Collider2D doorCollider;
    private List<Collider2D> collidersInside;
    public bool forcedOpen = false;
    public bool malfunction = false;
    public float malfTime = 1.0f;
    private float malfTimer;
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        malfTimer = Time.time+ malfTime;
        collidersInside = new List<Collider2D>();
        foreach (Collider2D i in this.GetComponents<Collider2D>())
        {
            if (!i.isTrigger)
            {
                doorCollider = i;
            }

        }

        //check has power script add if not found
        //run check for power

    }

    // Update is called once per frame
    void Update()
    {
       
    }
    void FixedUpdate(){
        // not final but here to simulate effect
        if (!powerOn && !malfunction)
        {
            malfunction = true;
            malfTimer = Time.time + malfTime;
        }
        else if(powerOn)
        {
            malfunction = false;
        }
         if (malfunction)
        {
            if (Time.time >= malfTimer)
            {
                doorCollider.enabled = !doorCollider.enabled;
                Debug.Log("malfunction");
                this.GetComponent<SpriteRenderer>().enabled = !this.GetComponent<SpriteRenderer>().enabled;
                if (doorCollider.enabled)
                {
                    KillPlayers();
                }
                malfTimer = Time.time + malfTime;
            }
         // if timer elapsed
                //change state
                // if close kill every thing inside colldier

         //reset timer
        }
    }
    public void KillPlayers()
    {
        foreach(Collider2D c in collidersInside)
        {
            if (c.gameObject.GetComponent<Player_Controller>() != null)
            {
                // kill player()
                //remove collider from list
                Debug.Log("player died");
                c.gameObject.GetComponent<Player_Controller>().playerTakeDamage(100);
                collidersInside.Remove(c);
            }
        }
    }
    /// <summary>
    /// changes doors physics to let players through
    /// </summary>
    /// <param name="open"></param>
    public void ChangeDoorState(bool open)
    {
        if (forcedOpen)
        {
            return;
        }
        if (powerOn) {
            if (open) {
                doorCollider.enabled = false;
                Debug.Log("open");
                this.GetComponent<SpriteRenderer>().enabled = false;
            }
            else
            {

                doorCollider.enabled = true;
                Debug.Log("closed");
                this.GetComponent<SpriteRenderer>().enabled = true;
            }

        }
    }
    public void Force()
    {
        if (!powerOn)
        {
            forcedOpen = true;
            doorCollider.enabled = false;
        }
    }
    void  OnTriggerEnter2D(Collider2D other)

    {
        if (other.tag == "Player")
        {
            collidersInside.Add(other);
            ChangeDoorState(true);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player") { 
        collidersInside.Remove(other);
        if (collidersInside.Count == 0)
        {
            ChangeDoorState(false);

        }
    }
        
    }
    public override void PowerUp()
    {
        base.PowerUp();
        malfunction = false;

        doorCollider.enabled = true;
        Debug.Log("closed");
        this.GetComponent<SpriteRenderer>().enabled = true;

    }
}

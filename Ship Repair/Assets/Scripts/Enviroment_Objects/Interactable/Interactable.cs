﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : Enviroment_Object {

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void interact(ref GameObject gameObject)
    {
        //do interaction
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenuITem : MonoBehaviour {

	// Use this for initialization
	void Start () {
       this.GetComponent<CanvasRenderer>().SetAlpha(0f);
        transform.parent.gameObject.GetComponent<UI_Controller>().MiddleMenu = this.gameObject;
        transform.parent.gameObject.GetComponent<UI_Controller>().menuObj.Add(this.gameObject);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
